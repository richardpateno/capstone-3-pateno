import { Carousel } from "antd";
import HomeWrapper from "../components/HomeWrapper";

const contentStyle = {
  height: "160px",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

export default function Home() {
  return <HomeWrapper></HomeWrapper>;
}
