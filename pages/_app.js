import "bootstrap/dist/css/bootstrap.min.css";
import "../styles/globals.css";
import "antd/dist/antd.css";

//hooks from react
import { useState, useEffect } from "react";

//import Navbar in the app component
import NavBar from "../components/NavBar";

//import our UserProvider
import { UserProvider } from "../userContext";

/*
	NextJS uses the App component to initialize our pages. However, routing is already done by NextJS for us. Here in the App.js our component is received as a prop. The active page is the Component prop received in this App component. If you console log the component you will be able to see the content of the active page.
*/
function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({
    email: null,
    isAdmin: null,
  });

  /*
    NextJS is pre-rendered. Your pages are built initially in the server then passed into the browser. localStorage does not exist until the page/component is rendered. We are going to use useEffect to get our email and isAdmin details from our localStorage instead so as to assure that we access the localStorage only after the page/component has initially rendered.

  */

  useEffect(() => {
    setUser({
      email: localStorage.getItem("email"),
      isAdmin: localStorage.getItem("isAdmin") === "true",
    });
  }, []);

  const unsetUser = () => {
    //clear the localStorage
    localStorage.clear();

    //set the values of our state back to its initial value
    setUser({
      email: null,
      isAdmin: null,
    });
  };

  /*
    We'd like to pass the user state, the setter function for user state and the unsetUser function to all of our components. How will we pass these items?
  */

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <NavBar />
        <Component {...pageProps} />
      </UserProvider>
    </>
  );
}

export default MyApp;
