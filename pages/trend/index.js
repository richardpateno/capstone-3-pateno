import React, { useState, useEffect } from "react";
import { Row, Col, Typography, DatePicker } from "antd";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import MonthlyWrapper from "../../components/MonthlyWrapper";

const { Title } = Typography;
const { RangePicker } = DatePicker;

const Trend = () => {
  const [totalIncome, setTotalIncome] = useState(0);
  const [expenses, setExpenses] = useState([]);
  const [wallet, setWallet] = useState([]);

  useEffect(() => {
    let token = localStorage.getItem("token");

    fetch("https://arcane-castle-99593.herokuapp.com/api/items", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        let totalIncomeData = 0;
        data.forEach((entry) => {
          if (entry.category.type === "Income") totalIncomeData += entry.price;
        });
        setTotalIncome(totalIncomeData);

        const expensesArr = data.filter(
          (item) => item.category.type === "Expense"
        );
        setExpenses(expensesArr);
      });
  }, []);

  useEffect(() => {
    const walletArr = [{ amount: totalIncome }];
    let currentWallet = totalIncome;
    expenses.forEach((expense) => {
      currentWallet -= expense.price;
      walletArr.push({ amount: currentWallet });
    });
    setWallet(walletArr);
  }, [expenses, totalIncome]);

  const handleDateChange = (date, dateString) => {
    let token = localStorage.getItem("token");
    fetch(
      `https://arcane-castle-99593.herokuapp.com/api/items?startDate=${dateString[0]}&endDate=${dateString[1]}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        let totalIncomeData = 0;
        data.forEach((entry) => {
          if (entry.category.type === "Income") totalIncomeData += entry.price;
        });
        setTotalIncome(totalIncomeData);

        const expensesArr = data.filter(
          (item) => item.category.type === "Expense"
        );
        setExpenses(expensesArr);
      });
  };

  return (
    <MonthlyWrapper>
      <Row justify="center" className="chart-container">
        <Col span={18} className="date-range-container">
          <Title level={3}>Wallet Trend</Title>
          <RangePicker onChange={handleDateChange} />
        </Col>
        <Col span={18}>
          <ResponsiveContainer width="100%" height="100%">
            <LineChart
              width={300}
              height={100}
              data={wallet}
              margin={{
                top: 5,
                right: 30,
                left: 20,
                bottom: 5,
              }}
            >
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="amount" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Line
                type="monotone"
                dataKey="amount"
                stroke="#8884d8"
                strokeWidth={2}
              />
            </LineChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </MonthlyWrapper>
  );
};

export default Trend;
