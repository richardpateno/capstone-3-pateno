import { useState, useEffect, useContext } from "react";
import { Form, Input, Button, Row, Col } from "antd";
import Swal from "sweetalert2";
import Router from "next/router";
import UserContext from "../../userContext";
import { GoogleLogin } from "react-google-login";
import LoginWrapper from "../../components/LoginWrapper";

export default function Login() {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isActive, setIsActive] = useState(true);

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  function authenticate(values) {
    fetch("https://arcane-castle-99593.herokuapp.com/api/users/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: values.email,
        password: values.password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.accessToken) {
          localStorage.setItem("token", data.accessToken);
          fetch("https://arcane-castle-99593.herokuapp.com/api/users/details", {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              localStorage.setItem("email", data.email);
              localStorage.setItem("isAdmin", data.isAdmin);

              //after getting the user's details from the API server, we will set the global user state
              setUser({
                email: data.email,
                isAdmin: data.isAdmin,
              });
            });

          Swal.fire({
            icon: "success",
            title: "Successfully Logged In.",
            text: "Thank you for logging in.",
          });

          Router.push("/");
        } else {
          Swal.fire({
            icon: "error",
            title: "Unsuccessful Login",
            text: "User authentication has failed.",
          });
        }
      });

    //set the input states into their initial value
    setEmail("");
    setPassword("");
  }

  function authenticateGoogleToken(response) {
    //google's response with our tokenId to be used to authenticate our google login user

    fetch(
      "https://arcane-castle-99593.herokuapp.com/api/users/verify-google-id-token",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          tokenId: response.tokenId,
          accessToken: response.accessToken,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        //we will show alerts to show if the user logged in properly or if there are errors.
        if (typeof data.accessToken !== "undefined") {
          //set the accessToken into our localStorage as token:
          localStorage.setItem("token", data.accessToken);

          //run a fetch request to get our user's details and update our global user state and save our user details into the localStorage:
          fetch("https://arcane-castle-99593.herokuapp.com/api/users/details", {
            headers: {
              Authorization: `Bearer ${data.accessToken}`,
            },
          })
            .then((res) => res.json())
            .then((data) => {
              localStorage.setItem("email", data.email);
              localStorage.setItem("isAdmin", data.isAdmin);

              //after getting the user's details, update the global user state:
              setUser({
                email: data.email,
                isAdmin: data.isAdmin,
              });

              //Fire a sweetalert to inform the user of successful login:
              Swal.fire({
                icon: "success",
                title: "Successful Login",
              });

              Router.push("/category");
            });
        } else {
          //if data.accessToken is undefined, therefore, data contains a property called error instead.
          if (data.error === "google-auth-error") {
            Swal.fire({
              icon: "error",
              title: "Google Authentication Failed",
            });
          } else if (data.error === "login-type-error") {
            Swal.fire({
              icon: "error",
              title: "Login Failed.",
              text: "You may have registered through a different procedure.",
            });
          }
        }
      });
  }

  //component return
  return (
    <LoginWrapper>
      <Row justify="center" className="login-container">
        <Col xs={20} md={12} lg={10}>
          <Form
            labelCol={{ span: 6 }}
            name="basic"
            initialValues={{ remember: true }}
            onFinish={authenticate}
          >
            <Form.Item
              label="Email"
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item className="submit-btn-container">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>

          <GoogleLogin
            clientId="656593662569-24gufu44evj4ujqs3k0113rgrurq2bnp.apps.googleusercontent.com"
            buttonText="Login Using Google"
            onSuccess={authenticateGoogleToken}
            onFailure={authenticateGoogleToken}
            cookiePolicy={"single_host_origin"}
            className="w-100 text-center d-flex justify-content-center"
          />
        </Col>
      </Row>
    </LoginWrapper>
  );
}
