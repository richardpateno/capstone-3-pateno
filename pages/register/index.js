import { useEffect, useState, useContext } from "react";
import { Form, Input, Button, Row, Col } from "antd";
import RegisterWrapper from "../../components/RegisterWrapper";

//import router from nextJS for user redirection
import Router from "next/router";

//import Swal
import Swal from "sweetalert2";

export default function Register() {
  /*state for conditionally rendering the submit button*/
  const [isActive, setIsActive] = useState(true);
  function registerUser(values) {
    //check if an email already exists
    fetch("https://arcane-castle-99593.herokuapp.com/api/users/email-exists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: values.email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === false) {
          fetch("https://arcane-castle-99593.herokuapp.com/api/users/", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: values.firstName,
              lastName: values.lastName,
              email: values.email,
              mobileNo: values.mobileNo,
              password: values.password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data) {
                Swal.fire({
                  icon: "success",
                  title: "Successfully Registered.",
                  text: "Thank you for registering.",
                });

                Router.push("/login");
              } else {
                Swal.fire({
                  icon: "error",
                  title: "Registration Failed",
                  text: "Something went wrong.",
                });
              }
            });
        } else {
          Swal.fire({
            icon: "error",
            title: "Registration Failed.",
            text: "Email Already Registered.",
          });
        }
      });
  }

  return (
    <RegisterWrapper>
      <Row justify="center" className="register-container">
        <Col xs={20} md={12} lg={10}>
          <Form labelCol={{ span: 6 }} name="register" onFinish={registerUser}>
            <Form.Item
              label="First Name"
              name="firstName"
              rules={[
                { required: true, message: "Please input your first name!" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Last Name"
              name="lastName"
              rules={[
                { required: true, message: "Please input your last name!" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              rules={[{ required: true, message: "Please input your email!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Mobile Number"
              name="mobileNo"
              rules={[
                { required: true, message: "Please input your mobile number!" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item className="submit-btn-container">
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </RegisterWrapper>
  );
}
