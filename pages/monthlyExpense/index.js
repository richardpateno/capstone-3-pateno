import React, { useState, useEffect } from "react";
import { Row, Col, Typography } from "antd";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import _ from "lodash";
import MonthlyWrapper from "../../components/MonthlyWrapper";
import moment from "moment";

const { Title } = Typography;

const MonthlyExpense = () => {
  const initialData = [
    {
      month: "January",
      expense: 0,
    },
    {
      month: "February",
      expense: 0,
    },
    {
      month: "March",
      expense: 0,
    },
    {
      month: "April",
      expense: 0,
    },
    {
      month: "May",
      expense: 0,
    },
    {
      month: "June",
      expense: 0,
    },
    {
      month: "July",
      expense: 0,
    },
    {
      month: "August",
      expense: 0,
    },
    {
      month: "September",
      expense: 0,
    },
    {
      month: "October",
      expense: 0,
    },

    {
      month: "November",
      expense: 0,
    },

    {
      month: "December",
      expense: 0,
    },
  ];

  const [monthlyExpense, setMonthlyExpense] = useState(initialData);

  useEffect(() => {
    let token = localStorage.getItem("token");
    fetch("https://arcane-castle-99593.herokuapp.com/api/items/expense", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const initialDataClone = [...initialData];
        data.forEach((entry) => {
          const index = initialDataClone.findIndex(
            (item) => item.month === moment(entry.createdAt).format("MMMM")
          );
          initialDataClone[index].expense += entry.price;
        });
        setMonthlyExpense(initialDataClone);
      });
  }, []);

  return (
    <MonthlyWrapper>
      <Row justify="center" className="chart-container">
        <Col span={18} className="date-range-container">
          <Title level={3}>Monthly Expense</Title>
        </Col>
        <Col span={18}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart width={150} height={40} data={monthlyExpense}>
              <XAxis dataKey="month" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="expense" fill="#8884d8" />
            </BarChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </MonthlyWrapper>
  );
};

export default MonthlyExpense;
