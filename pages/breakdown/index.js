import React, { useState, useEffect } from "react";
import { Row, Col, Typography, DatePicker } from "antd";
import {
  PieChart,
  Pie,
  Legend,
  Tooltip,
  ResponsiveContainer,
  Cell,
} from "recharts";
import _ from "lodash";
import MonthlyWrapper from "../../components/MonthlyWrapper";
import moment from "moment";

const { Title } = Typography;
const { RangePicker } = DatePicker;

const data01 = [
  { name: "Group A", value: 400 },
  { name: "Group B", value: 300 },
  { name: "Group C", value: 300 },
  { name: "Group D", value: 200 },
  { name: "Group E", value: 278 },
  { name: "Group F", value: 189 },
];

const COLORS = [
  "#0088FE",
  "#00C49F",
  "#FFBB28",
  "#FF8042",
  "#e57373",
  "#ba68c8",
  "#90caf9",
  "#4db6ac",
  "#dce775",
  "#ffb74d",
  "#b0bec5",
  "#81c784",
];

const Breakdown = () => {
  const [categoryPrice, setCategoryPrice] = useState([]);

  useEffect(() => {
    let token = localStorage.getItem("token");
    fetch(`https://arcane-castle-99593.herokuapp.com/api/items`, {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const categoryPriceArr = [];
        data.forEach((entry) => {
          const foundIndex = categoryPriceArr.findIndex((categoryEntry) => {
            if (categoryEntry) {
              return categoryEntry.name === entry.category.name;
            }
          });
          if (foundIndex === -1) {
            categoryPriceArr.push({
              name: entry.category.name,
              value: entry.price,
            });
          } else {
            categoryPriceArr[foundIndex].value += entry.price;
          }
        });

        setCategoryPrice(categoryPriceArr);
      });
  }, []);

  const handleDateChange = (date, dateString) => {
    let token = localStorage.getItem("token");
    fetch(
      `https://arcane-castle-99593.herokuapp.com/api/items?startDate=${dateString[0]}&endDate=${dateString[1]}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    )
      .then((res) => res.json())
      .then((data) => {
        const categoryPriceArr = [];
        data.forEach((entry) => {
          const foundIndex = categoryPriceArr.findIndex((categoryEntry) => {
            if (categoryEntry) {
              return categoryEntry.name === entry.category.name;
            }
          });
          if (foundIndex === -1) {
            categoryPriceArr.push({
              name: entry.category.name,
              value: entry.price,
            });
          } else {
            categoryPriceArr[foundIndex].value += entry.price;
          }
        });
        setCategoryPrice(categoryPriceArr);
      });
  };
  return (
    <MonthlyWrapper>
      <Row justify="center" className="chart-container">
        <Col span={18} className="date-range-container">
          <Title level={3}>Category Breakdown</Title>
          <RangePicker onChange={handleDateChange} />
        </Col>
        <Col span={18}>
          <ResponsiveContainer width="100%" height="100%">
            <PieChart width={400} height={400}>
              <Pie
                dataKey="value"
                isAnimationActive={false}
                data={categoryPrice}
                cx="50%"
                cy="50%"
                outerRadius={80}
                fill="#8884d8"
                label
              >
                {categoryPrice.map((entry, index) => (
                  <Cell
                    key={`cell-${index}`}
                    fill={COLORS[index % COLORS.length]}
                  />
                ))}
              </Pie>
              <Tooltip />
            </PieChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </MonthlyWrapper>
  );
};

export default Breakdown;
