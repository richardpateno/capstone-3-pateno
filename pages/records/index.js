import React, { useContext, useState, useEffect } from "react";
import {
  Button,
  Row,
  Col,
  Table,
  Space,
  Modal,
  Form,
  Input,
  Select,
  Popconfirm,
  InputNumber,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import CategoryWrapper from "../../components/CategoryWrapper";

export default function Categories() {
  const [categories, setCategories] = useState([]);
  const [records, setRecords] = useState([]);
  const [recordId, setRecordId] = useState(null);
  const [initialValues, setInitialValues] = useState(null);
  const [modalTitle, setModalTitle] = useState("Add Record");
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [form] = Form.useForm();

  const showModal = (title, _id) => {
    setModalTitle(title);
    setIsOpenModal(true);
    if (_id) {
      setRecordId(_id);
    } else {
      setRecordId(null);
    }
  };

  const handleCancel = () => {
    setIsOpenModal(false);
  };

  const addOrUpdateRecord = (values) => {
    if (recordId) {
      fetch("https://arcane-castle-99593.herokuapp.com/api/items/", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          _id: recordId,
          name: values.name,
          price: values.price,
          category: values.category,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          const newRecords = records.filter(
            (record) => record._id !== data._id
          );
          setRecords([...newRecords, data]);
        });
    } else {
      fetch("https://arcane-castle-99593.herokuapp.com/api/items/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: values.name,
          price: values.price,
          category: values.category,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setRecords([...records, data]);
        });
    }

    setIsOpenModal(false);
  };

  function deleteRecord(_id) {
    fetch(`https://arcane-castle-99593.herokuapp.com/api/items/${_id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then(() => {
        setRecords(records.filter((record) => record._id !== _id));
      });

    setIsOpenModal(false);
  }

  useEffect(() => {
    if (recordId && modalTitle === "Edit Record") {
      const record = records.find(
        (recordEntry) => recordEntry._id === recordId
      );
      setInitialValues(record);
      form.setFieldsValue({
        name: record.name,
        price: record.price,
        category: record.category.name,
      });
    } else {
      setInitialValues(null);
      form.setFieldsValue({ name: "", price: "", category: "" });
    }
  }, [modalTitle, recordId]);

  useEffect(() => {
    let token = localStorage.getItem("token");
    fetch("https://arcane-castle-99593.herokuapp.com/api/items/", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setRecords(data);
      });

    fetch("https://arcane-castle-99593.herokuapp.com/api/categories/", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCategories(data);
      });
  }, []);

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Category",
      dataIndex: ["category", "name"],
    },
    {
      title: "Type",
      dataIndex: ["category", "type"],
    },
    {
      title: "Price",
      dataIndex: "price",
    },
    {
      title: "Actions",
      dataIndex: "_id",
      key: "actions",
      align: "center",
      render: (_id) => {
        return (
          <Space>
            <Button
              type="link"
              onClick={() => showModal("Edit Record", _id)}
              icon={<EditOutlined />}
            />
            <Popconfirm
              icon={<QuestionCircleOutlined style={{ color: "red" }} />}
              okText="Yes"
              onConfirm={() => deleteRecord(_id)}
              placement="topRight"
              title={`Do you want to delete this record?`}
            >
              <Button type="link" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <CategoryWrapper>
      <Row justify="center" className="category-container">
        <Col xs={22} md={20} lg={16}>
          <Space style={{ marginBottom: 16 }}>
            <Button type="primary" onClick={() => showModal("Add Record")}>
              Add Record
            </Button>
          </Space>
          <Table columns={columns} dataSource={records} />
        </Col>
      </Row>
      <Modal
        title={modalTitle}
        visible={isOpenModal}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          form={form}
          labelCol={{ span: 6 }}
          name="records"
          onFinish={addOrUpdateRecord}
          initialValues={initialValues}
        >
          <Form.Item
            label="Record Name"
            name="name"
            rules={[{ required: true, message: "Please input a Record name!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Record price"
            name="price"
            rules={[
              { required: true, message: "Please input a Record price!" },
            ]}
          >
            <InputNumber />
          </Form.Item>
          <Form.Item
            label="Category"
            name="category"
            rules={[{ required: true, message: "Please select a category!" }]}
          >
            <Select style={{ width: 200 }} placeholder="Select category type">
              {categories.map((category) => (
                <Select.Option value={category.name}>
                  {category.name} - {category.type}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item className="submit-btn-container">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </CategoryWrapper>
  );
}
