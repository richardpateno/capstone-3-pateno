/*
	NextJS follows your file path/folder structure for its routing and the endpoint for this page is now:
	
	/courses/create
*/
import { useEffect, useState, useContext } from "react";
import { Form, Button } from "react-bootstrap";

//import router from nextJS for user redirection
import Router from "next/router";

//import Swal
import Swal from "sweetalert2";

export default function CreateCourse() {
  //input states
  const [courseName, setCourseName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);

  //state for button conditional rendering
  const [isActive, setIsActive] = useState(true);

  //check our user's input
  useEffect(() => {
    if (courseName !== "" && description !== "" && price !== 0) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [courseName, description, price]);

  function addCourse(e) {
    e.preventDefault();

    let token = localStorage.getItem("token");
    /*
			Console everything you need to pass to the server, before passing it.
		*/
    fetch("https://arcane-castle-99593.herokuapp.com/api/courses/", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        name: courseName,
        description: description,
        price: price,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            icon: "success",
            title: "Course Added.",
            text: "Course successfully added.",
          });

          Router.push("/courses");
        } else {
          Swal.fire({
            icon: "error",
            title: "Course Creation Failed",
            text: "Something went wrong.",
          });
        }
      });

    setCourseName("");
    setDescription("");
    setPrice(0);
  }

  return (
    <>
      <h1 className="text-center">Create Course</h1>
      <Form onSubmit={(e) => addCourse(e)}>
        <Form.Group controlId="courseName">
          <Form.Label>Course Name</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Course Name"
            value={courseName}
            onChange={(e) => setCourseName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="price">
          <Form.Label>Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter Price"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>

        {isActive ? (
          <Button variant="primary" type="submit">
            Add Course
          </Button>
        ) : (
          <Button variant="primary" disabled>
            Add Course
          </Button>
        )}
      </Form>
    </>
  );
}
