import React, { useContext, useState, useEffect } from "react";
import {
  Button,
  Row,
  Col,
  Table,
  Space,
  Modal,
  Form,
  Input,
  Select,
  Popconfirm,
} from "antd";
import {
  EditOutlined,
  DeleteOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import CategoryWrapper from "../../components/CategoryWrapper";

export default function Categories() {
  const [categories, setCategories] = useState([]);
  const [categoryId, setCategoryId] = useState(null);
  const [initialValues, setInitialValues] = useState(null);
  const [modalTitle, setModalTitle] = useState("Add Category");
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [form] = Form.useForm();

  const showModal = (title, _id) => {
    setModalTitle(title);
    setIsOpenModal(true);
    if (_id) {
      setCategoryId(_id);
    } else {
      setCategoryId(null);
    }
  };

  const handleCancel = () => {
    setIsOpenModal(false);
  };

  function addOrUpdateCategory(values) {
    if (categoryId) {
      fetch("https://arcane-castle-99593.herokuapp.com/api/categories/", {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          _id: categoryId,
          name: values.name,
          type: values.type,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          const newCategories = categories.filter(
            (category) => category._id !== data._id
          );
          setCategories([...newCategories, data]);
        });
    } else {
      fetch("https://arcane-castle-99593.herokuapp.com/api/categories/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          name: values.name,
          type: values.type,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          setCategories([...categories, data]);
        });
    }

    setIsOpenModal(false);
  }

  function deleteCategory(_id) {
    fetch(`https://arcane-castle-99593.herokuapp.com/api/categories/${_id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then(() => {
        setCategories(categories.filter((category) => category._id !== _id));
      });

    setIsOpenModal(false);
  }

  useEffect(() => {
    if (categoryId && modalTitle === "Edit Category") {
      const category = categories.find(
        (categoryEntry) => categoryEntry._id === categoryId
      );
      setInitialValues(category);
      form.setFieldsValue(category);
    } else {
      setInitialValues(null);
      form.setFieldsValue({ name: "", type: "" });
    }
  }, [modalTitle, categoryId]);

  useEffect(() => {
    let token = localStorage.getItem("token");
    fetch("https://arcane-castle-99593.herokuapp.com/api/categories/", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCategories(data);
      });
  }, []);

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
    },
    {
      title: "Type",
      dataIndex: "type",
    },
    {
      title: "Actions",
      dataIndex: "_id",
      key: "actions",
      align: "center",
      render: (_id) => {
        return (
          <Space>
            <Button
              type="link"
              onClick={() => showModal("Edit Category", _id)}
              icon={<EditOutlined />}
            />
            <Popconfirm
              icon={<QuestionCircleOutlined style={{ color: "red" }} />}
              okText="Yes"
              onConfirm={() => deleteCategory(_id)}
              placement="topRight"
              title={`Do you want to delete this category?`}
            >
              <Button type="link" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Space>
        );
      },
    },
  ];

  return (
    <CategoryWrapper>
      <Row justify="center" className="category-container">
        <Col xs={22} md={20} lg={16}>
          <Space style={{ marginBottom: 16 }}>
            <Button type="primary" onClick={() => showModal("Add Category")}>
              Add Category
            </Button>
          </Space>
          <Table columns={columns} dataSource={categories} />
        </Col>
      </Row>
      <Modal
        title={modalTitle}
        visible={isOpenModal}
        footer={null}
        onCancel={handleCancel}
      >
        <Form
          form={form}
          labelCol={{ span: 6 }}
          name="category"
          onFinish={addOrUpdateCategory}
          initialValues={initialValues}
        >
          <Form.Item
            label="Category Name"
            name="name"
            rules={[
              { required: true, message: "Please input a category name!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Category Type"
            name="type"
            rules={[
              { required: true, message: "Please input a category type!" },
            ]}
          >
            <Select style={{ width: 200 }} placeholder="Select category type">
              <Select.Option value="Income">Income</Select.Option>
              <Select.Option value="Expense">Expense</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item className="submit-btn-container">
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </CategoryWrapper>
  );
}
