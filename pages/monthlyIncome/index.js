import React, { useState, useEffect } from "react";
import { Row, Col, Typography } from "antd";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import _ from "lodash";
import MonthlyWrapper from "../../components/MonthlyWrapper";
import moment from "moment";

const { Title } = Typography;

const MonthlyIncome = () => {
  const initialData = [
    {
      month: "January",
      income: 0,
    },
    {
      month: "February",
      income: 0,
    },
    {
      month: "March",
      income: 0,
    },
    {
      month: "April",
      income: 0,
    },
    {
      month: "May",
      income: 0,
    },
    {
      month: "June",
      income: 0,
    },
    {
      month: "July",
      income: 0,
    },
    {
      month: "August",
      income: 0,
    },
    {
      month: "September",
      income: 0,
    },
    {
      month: "October",
      income: 0,
    },

    {
      month: "November",
      income: 0,
    },

    {
      month: "December",
      income: 0,
    },
  ];

  const [monthlyIncome, setMonthlyIncome] = useState(initialData);

  useEffect(() => {
    let token = localStorage.getItem("token");
    fetch("https://arcane-castle-99593.herokuapp.com/api/items/income", {
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        const initialDataClone = [...initialData];
        data.forEach((entry) => {
          const index = initialDataClone.findIndex(
            (item) => item.month === moment(entry.createdAt).format("MMMM")
          );
          initialDataClone[index].income += entry.price;
        });
        setMonthlyIncome(initialDataClone);
      });
  }, []);

  return (
    <MonthlyWrapper>
      <Row justify="center" className="chart-container">
        <Col span={18} className="date-range-container">
          <Title level={3}>Monthly Income</Title>
        </Col>
        <Col span={18}>
          <ResponsiveContainer width="100%" height="100%">
            <BarChart width={150} height={40} data={monthlyIncome}>
              <XAxis dataKey="month" />
              <YAxis />
              <Tooltip />
              <Legend />
              <Bar dataKey="income" fill="#8884d8" />
            </BarChart>
          </ResponsiveContainer>
        </Col>
      </Row>
    </MonthlyWrapper>
  );
};

export default MonthlyIncome;
