import styled from 'styled-components';

export default styled.div`
	.login-container {
		margin-top: 4rem;
	}

	.submit-btn-container {
		text-align: center;
	}
`;
