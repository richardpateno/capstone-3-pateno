import styled from 'styled-components';

export default styled.div`
	.chart-container {
		margin-top: 4rem;
		width: 100vw;
		min-height: 50vh;
	}

	.date-range-container {
		text-align: center;
	}
`;
