import { Jumbotron, Row, Col } from "react-bootstrap";

//Link component from nextjs for navigation
import Link from "next/link";

export default function Banner({ dataProp }) {
  //console.log(dataProp)
  const { title, content, destination, label } = dataProp;

  /*
		Link component is used in NextJS to navigate through pages. It uses NextJS routing and can wrap an anchor tag. NextJS Link component has an href attribute which acts just like our anchor tag href attribute. However, the child <a> tag of the Link component is where any styling is applied.
	*/

  return (
    <Row>
      <Col>
        <Jumbotron>
          <h1>{title}</h1>
          <h5>{content}</h5>
          <h3>
            <Link href={destination}>
              <a>{label}</a>
            </Link>
          </h3>
        </Jumbotron>
      </Col>
    </Row>
  );
}
