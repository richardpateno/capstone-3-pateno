import styled from 'styled-components';

export default styled.div`
	.title {
		margin-top: 1rem;
		margin-left: 1rem;
		margin-bottom: 0;
	}
`;
