import { Row, Col, Card } from "react-bootstrap";

export default function Highlights() {
  return (
    <Row>
      <Col>
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Safe from kupit</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum duis reprehenderit amet est fugiat enim quis non
              laboris pariatur dolor veniam velit minim commodo magna nisi sed
              qui amet culpa laborum tempor in deserunt ex dolore duis.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col>
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Secure like Pentagon</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum duis reprehenderit amet est fugiat enim quis non
              laboris pariatur dolor veniam velit minim commodo magna nisi sed
              qui amet.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col>
        <Card className="cardHighlight">
          <Card.Body>
            <Card.Title>
              <h2>Friendly User</h2>
            </Card.Title>
            <Card.Text>
              Lorem ipsum duis reprehenderit amet est fugiat enim quis non
              laboris pariatur dolor veniam velit.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
