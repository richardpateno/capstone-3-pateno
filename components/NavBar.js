import React, { useContext, useState } from "react";
import { Navbar, Nav } from "react-bootstrap";
import { Menu, Typography } from "antd";

import {
  HomeOutlined,
  ReconciliationOutlined,
  UserOutlined,
  LineChartOutlined,
  AreaChartOutlined,
  FallOutlined,
  BookOutlined,
  DatabaseOutlined,
  SwapRightOutlined,
} from "@ant-design/icons";
//import Link component from nextjs
import Link from "next/link";
import { useRouter } from "next/router";
import UserContext from "../userContext";
import NavBarWrapper from "./NavBarWrapper";

const { Title } = Typography;

export default function NavBar() {
  const { user } = useContext(UserContext);
  const [current, setCurrent] = useState("home");
  const router = useRouter();

  const handleClick = (e) => {
    setCurrent(e.key);
    if (e.key === "home") {
      router.push("/");
    } else {
      router.push(e.key);
    }
  };

  const renderNotLoggedMenu = () => {
    return (
      <>
        <Menu.Item key="home" icon={<HomeOutlined />}>
          Home
        </Menu.Item>
        <Menu.Item key="register" icon={<ReconciliationOutlined />}>
          Register
        </Menu.Item>
        <Menu.Item key="login" icon={<UserOutlined />}>
          Login
        </Menu.Item>
      </>
    );
  };

  const renderLoggedMenu = () => {
    return (
      <>
        <Menu.Item key="home" icon={<HomeOutlined />}>
          Home
        </Menu.Item>
        <Menu.Item key="category" icon={<DatabaseOutlined />}>
          Category
        </Menu.Item>
        <Menu.Item key="records" icon={<BookOutlined />}>
          Record
        </Menu.Item>
        <Menu.Item key="monthlyExpense" icon={<AreaChartOutlined />}>
          Monthly Expense
        </Menu.Item>
        <Menu.Item key="monthlyIncome" icon={<AreaChartOutlined />}>
          Monthly Income
        </Menu.Item>
        <Menu.Item key="trend" icon={<LineChartOutlined />}>
          Trend
        </Menu.Item>
        <Menu.Item key="breakdown" icon={<FallOutlined />}>
          Breakdown
        </Menu.Item>
        <Menu.Item key="logout" icon={<SwapRightOutlined />}>
          Logout
        </Menu.Item>
      </>
    );
  };

  return (
    <NavBarWrapper>
      <Title level={3} className="title">
        Budget Tracking Supply
      </Title>
      <Menu onClick={handleClick} selectedKeys={[current]} mode="horizontal">
        {user.email ? renderLoggedMenu() : renderNotLoggedMenu()}
      </Menu>
    </NavBarWrapper>
  );
}
