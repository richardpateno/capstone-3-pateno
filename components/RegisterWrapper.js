import styled from 'styled-components';

export default styled.div`
	.register-container {
		margin-top: 4rem;
	}

	.submit-btn-container {
		text-align: right;
	}
`;
